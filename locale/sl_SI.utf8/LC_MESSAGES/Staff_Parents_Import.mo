��    )      d  ;   �      �     �     �     �  T   �  U   %     {     �     �     �     �  
   �     �     �  	   �  	   �     �  	          U   1     �     �     �     �      �  
   �     �     �        &   .     U     j     �     �     �     �     �     �     �     �  4   �  �        �  	   	  !   	  e   4	  j   �	     
     "
  !   4
     V
     \
     i
     m
     ~
     �
     �
  %   �
  
   �
     �
  l   �
  	   \     f     m     s  $   �     �     �     �  '   �  +   "     N     c     {     �  
   �     �     �     �     �     �  ;   �     )                    &       '                                     $             !                                                  #          "                (      	      %   
                          %d notifications sent. %s rows %s users were imported. A user with that username already exists. Choose a different username and try again. Are you absolutely ready to import users? Make sure you have backed up your database! Cannot open file. Checkbox Database Backup Date Email Address First Name Import Users Import first row Last Name Long Text Maximum file size: %01.0fMb No Access No names were found. Notifications are sent if Username, Password, User Profile and Email Address are set. Number Parent Password Phone Number Please enter valid Numeric data. Reset form Select CSV or Excel file Select Multiple from Options Send email notification to Users Some dates were not entered correctly. Staff Parents Import Staff and Parents Import Stop Submit Teacher Text User Account User Fields User Profile Username Your account was activated (%d). You can login at %s Project-Id-Version: Staff and Parents Import Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-15 15:52+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.3.2
X-Poedit-SearchPath-0: .
 %d poslanih obvestil. %s vrstic %s uporabnikov je bilo uvoženih. Uporabnik s tem uporabniškim imenom že obstaja. Izberite drugo uporabniško ime in poskusite znova. Ali ste povsem pripravljeni na uvoz uporabnikov? Prepričajte se, da ste varnostno kopirali bazo podatkov! Datoteke ni mogoče odpreti. Potrditveno polje Varnostno kopiranje baze podatkov Datum Email naslov Ime Uvozi uporabnike Uvozi prvo vrstico Priimek Dolgi tekst Največja velikost datoteke: %01.0fMb Ni dostopa Imena niso bila najdena. Obvestila se pošljejo, če so nastavljeni uporabniško ime, geslo, uporabniški profil in e-poštni naslov. Številka Starš Geslo Telefonska številka Vnesite veljavne številske podatke. Ponastavi obrazec Izberite datoteko CSV ali Excel V možnostih izberite Več Pošlji e-poštno obvestilo uporabnikom Nekateri datumi niso bili pravilno vneseni. Uvoz staršev osebja Uvoz osebja in staršev Ustavi Vpiši Vzgojitelj Tekst Uporabniški račun Uporabniška polja Uporabniški profil Uporabniško ime Vaš račun je bil aktiviran (%d). Prijavite se lahko na %s 